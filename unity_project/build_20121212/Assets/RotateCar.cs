using UnityEngine;
using System.Collections;

public class RotateCar : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
		if(this.renderer.enabled){
			this.transform.Rotate(Vector3.up * Time.deltaTime * 60, Space.World);
		}
		
	}
}
