using UnityEngine;
using System.Collections;

public class AppController : MonoBehaviour {
		
	/**
	 * ------------------------------------------------------------------
	 * Hot Wheels Vars
	 * ------------------------------------------------------------------
	 **/
	public GUITexture startButton;
	public bool startHasBeenPressed = false;
	private bool startButtonEnabled = false;  // was true
	
	public GUITexture slowMoButton;
	private bool slowMoButtonEnabled = false;
	
	public GUITexture launchButton;
	private bool launchButtonEnabled = false;
	
	public GUITexture moreInfoButton;
	private bool moreInfoButtonEnabled = false;
	
	public GUITexture hotWheelsBuyNowButton;
	private bool hotWheelsBuyNowButtonEnabled = false;
	
	public GameObject threeDModel;
	//private bool threeDModelVisible = false;
	private bool animationWaitFlag = false;
	
	public RotateCar rotatingCar;
	private bool rotatingCarVisible = false; // was true
	
	public GameObject slowMoModel;
	private bool slowMoModelVisible = false;
	

		
	
	/**
	 * ------------------------------------------------------------------
	 * Scene Vars
	 * ------------------------------------------------------------------
	 **/
	public SoundController soundController;
	
	public int framesPerSecond = 30;
	
	public TrackableEventHandler trackEventHandler;
	
	// Use this for initialization
	void Start () {
		//startButton.enabled = startButtonEnabled;
		//launchButton.enabled = launchButtonEnabled;
		//moreInfoButton.enabled = moreInfoButtonEnabled;
		//slowMoButton.enabled = slowMoButtonEnabled;
		//hotWheelsBuyNowButton.enabled = hotWheelsBuyNowButtonEnabled;	
		
		startButton.enabled = false;
		launchButton.enabled = false;
		moreInfoButton.enabled = false;
		slowMoButton.enabled = false;
		hotWheelsBuyNowButton.enabled = false;	
		
		// force start to not be pressed
		startHasBeenPressed = false;
					
		// Hide Hot Wheels Animations (using methods)
		// HideRotatingCar();
		HideSlowMoModel();
		
		// ALT: Hide Hotwheels Animations manually
		rotatingCarVisible = false;
		iTween.FadeFrom(rotatingCar.gameObject, iTween.Hash("alpha", 0f,"amount", 1f, "time", 0.0f));
		
		soundController.PlayIntroSound();
	}
	
	// Update is called once per frame
	void Update () {
		
		AnimationState animState;		
		int soundTriggerFrame;
		
		// set animation speed
		if (slowMoModelVisible) {			
			animState = slowMoModel.animation["Take 001"];
			animState.speed = 0.5f;	
			soundTriggerFrame = 80; // adjust sound slightly for slow-mo
		} else {
			animState = threeDModel.animation["Take 001"];	
			soundTriggerFrame = 73;
		} 
		
		float curAnimTime = animState.time;
		float curAnimFrame = curAnimTime * framesPerSecond;
		float roundedFrame = Mathf.Round(curAnimFrame);
		
		if(roundedFrame >= 210){
			threeDModel.animation.Stop();
			slowMoModel.animation.Stop();
			EnableLaunchButton();
			EnableMoreInfoButton();
			EnableSlowMoButton();
			EnableHotWheelsBuyNowButton();
			
			// hide the models until a new launch/slo-mo
			HideSlowMoModel();
			HideThreeDModel();
			
			//ShowThreeDModel();
			soundController.PlayIdleSound();
		}

		if(roundedFrame == soundTriggerFrame){
			soundController.PlayCrashSound();
		}		
	}
	
	
	/**
	 * -------------------------------------------------------------------------
	 * Hot Wheels AR Methods
	 * ------------------------------------------------------------------------- 
	 **/
	
	public void ShowHotWheelsUI() {
		EnableStartButton();
		EnableHotWheelsBuyNowButton();
		ShowRotatingCar();
	}

	
	// Disables all Hot Wheels related UI elements
	public void HideHotWheelsUI() {
		
		// hide buttons (using Disable methods)
		DisableStartButton();
		DisableLaunchButton();
		DisableMoreInfoButton();
		DisableSlowMoButton();
		DisableHotWheelsBuyNowButton();
		
		// hide models
		HideSlowMoModel();
		if (rotatingCarVisible) {
			HideRotatingCar();
		}
		HideThreeDModel();
	}
	
	public void StartHasBeenPressed(){
		if(!startHasBeenPressed) startHasBeenPressed = true;	
	}
	
	public void StopAndResetAllAnimations() {
		threeDModel.animation.Rewind();
		threeDModel.animation.Stop();
		
		slowMoModel.animation.Rewind();
		slowMoModel.animation.Stop();
	}
	
	/**
	 * ---------------------------------------------------------------
	 * GUI Button Model Methods
	 * ---------------------------------------------------------------
	 **/
	public void EnableLaunchButton(){
		if(!launchButtonEnabled){
			launchButton.enabled = true;
			launchButtonEnabled = !launchButtonEnabled;
			iTween.FadeFrom(launchButton.gameObject, iTween.Hash("alpha", 1f,"amount", 0f, "time", 0.75f));
		}
	}
	
	public void DisableLaunchButton(){
		if(launchButtonEnabled){
			launchButton.enabled = false;
			launchButtonEnabled = !launchButtonEnabled;
		}
	}
	
	public void EnableMoreInfoButton(){
		if(!moreInfoButtonEnabled) moreInfoButton.enabled = true;
		moreInfoButtonEnabled = true;
		iTween.FadeFrom(moreInfoButton.gameObject, iTween.Hash("alpha", 1f,"amount", 0f, "time", 0.75f));
	}
	
	public void DisableMoreInfoButton(){
		if(moreInfoButtonEnabled){
			moreInfoButton.enabled = false;
			moreInfoButtonEnabled = false;
		}
	}
	
	public void EnableStartButton(){
		if(!startButtonEnabled) {
			startButton.enabled = true;	
			startButtonEnabled = true;
		}
	}
	
	public void DisableStartButton(){
		if(startButtonEnabled) {
			startButton.enabled = false;
			startButtonEnabled = false;
		}
	}
	
	public void EnableSlowMoButton(){
		if(!slowMoButtonEnabled) {
			slowMoButton.enabled = true;
			slowMoButtonEnabled = true;
		}
	}
	
	public void DisableSlowMoButton(){
		if(slowMoButtonEnabled) {
			slowMoButton.enabled = false;
			slowMoButtonEnabled = false;
		}
	}
	
	public void EnableHotWheelsBuyNowButton(){
		if(!hotWheelsBuyNowButtonEnabled) {
			hotWheelsBuyNowButton.enabled = true;
			hotWheelsBuyNowButtonEnabled = true;
		}
	}
	
	public void DisableHotWheelsBuyNowButton(){
		if(hotWheelsBuyNowButtonEnabled) {
			hotWheelsBuyNowButton.enabled = false;
			hotWheelsBuyNowButtonEnabled = false;
		}
	}
	
	
	/**
	 * ---------------------------------------------------------------
	 * Rotating Car Methods
	 * ---------------------------------------------------------------
	 **/
	public void ShowRotatingCar() {
		rotatingCarVisible = true;
		iTween.FadeFrom(rotatingCar.gameObject, iTween.Hash("alpha", 1f,"amount", 0f, "time", 0.75f, "oncomplete", "EnableRotatingCar"));
	}
	
	public void EnableRotatingCar(){
		if(!rotatingCarVisible){
			rotatingCar.renderer.enabled = true;
		}
	}
	
	public void HideRotatingCar(){
		rotatingCarVisible = false;
		iTween.FadeFrom(rotatingCar.gameObject, iTween.Hash("alpha", 0f,"amount", 1f, "time", 0.35f, "oncomplete", "DisableRotatingCar"));
	}
	
	public void DisableRotatingCar(){
		if(rotatingCarVisible){
			rotatingCar.renderer.enabled = false;
		}
	}
	

	/**
	 * ---------------------------------------------------------------
	 * ThreeD Loop Model Methods
	 * ---------------------------------------------------------------
	 **/
	public void ShowThreeDModel(){
		Renderer[] renderComponents = threeDModel.GetComponentsInChildren<Renderer>();
		foreach(Renderer r in renderComponents){
			r.enabled = true;
		}
		iTween.FadeFrom(threeDModel, iTween.Hash("alpha", 1f,"amount", 0f, "time", 0.75f));
		//threeDModelVisible = true;
	}
	
	public void HideThreeDModel(){
		Renderer[] renderComponents = threeDModel.GetComponentsInChildren<Renderer>();
		foreach(Renderer r in renderComponents){
			r.enabled = false;
		}
		// DisableThreeDModel();
	}
	
//	public void DisableThreeDModel() {
//		if (threeDModelVisible) {
//			threeDModel.renderer.enabled = false;
//			threeDModelVisible = false;
//		}
//	}
	
	
	/**
	 * ---------------------------------------------------------------
	 * SloMo Model Methods
	 * ---------------------------------------------------------------
	 **/
	public void ShowSlowMoModel(){
		Renderer[] renderComponents = slowMoModel.GetComponentsInChildren<Renderer>();
		foreach(Renderer r in renderComponents){
			r.enabled = true;
		}
		iTween.FadeFrom(slowMoModel, iTween.Hash("alpha", 1f,"amount", 0f, "time", 0.75f));
		slowMoModelVisible = true;
	}
	
	public void HideSlowMoModel(){
		Debug.Log ("Hiding SlowMoModel");
		Renderer[] renderComponents = slowMoModel.GetComponentsInChildren<Renderer>();
		foreach(Renderer r in renderComponents){
			r.enabled = false;
		}
		slowMoModelVisible = false;
	}

	public void DisableSlowMoModel(){
		if(slowMoModelVisible) {
			slowMoModel.renderer.enabled = false;
			slowMoModelVisible = false;
		}
	}
	
	public IEnumerator PlaySlowMoAnimation(){
		
		Debug.Log("PlaySlowMoAnimation Coroutine");
		
		while(animationWaitFlag == false){
	    	animationWaitFlag = true;
			yield return new WaitForSeconds(0.75f);
		}
		
		if(animationWaitFlag){
			Debug.Log("Starting Animation");
			slowMoModel.animation.Rewind();
			slowMoModel.animation.wrapMode = WrapMode.ClampForever;
			slowMoModel.animation.Play();
			yield return null;	
		}
		
	}
	
	public IEnumerator PlayModelAnimation(){
		
		Debug.Log("PlayModelAnimation Coroutine");
		
		while(animationWaitFlag == false){
	    	animationWaitFlag = true;
			yield return new WaitForSeconds(0.75f);
		}
		
		if(animationWaitFlag){
			Debug.Log("Starting Animation");
			threeDModel.animation.Rewind();
			threeDModel.animation.wrapMode = WrapMode.ClampForever;
			threeDModel.animation.Play();
			yield return null;	
		}
		
	}

	
	public void StopAllSounds() {
		soundController.StopAudio();
	}
		
	public void StartExperiance(){
		
	}
	
}
