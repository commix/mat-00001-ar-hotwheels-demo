using UnityEngine;
using System.Collections;

public class TouchController : MonoBehaviour {
	
	public AppController mainAppController;
	public GUITexture[] guiTextures = new GUITexture[5];
	public string mattelURL;
	public string barbieBuyURL;
	public string hotwheelsBuyURL;
	
	// Use this for initialization
	void Start () {
		Debug.Log("starting TouchController");
	}
	
	// Update is called once per frame
	void Update () {
		
		//check for touches
		int touchCount = Input.touchCount;
		
		for(int i = 0; i < touchCount; i++){
			
			Touch touch = Input.GetTouch(i);
			
			if(touch.phase == TouchPhase.Began){
				
				//loop over all gui textures to see if there is a hit
				for(var j = 0; j < guiTextures.Length; j++){
					
					GUITexture texture = guiTextures[j];
					Vector3 touchPos = new Vector3(touch.position.x, touch.position.y, 0f);
					
					//check for hittest and if the button is enabled
					if(GUIHitTest(texture, touchPos) && texture.enabled){
						//there is a hit, determine what to do
						switch(texture.name){
							case "start button":
								mainAppController.DisableStartButton();
								mainAppController.HideRotatingCar();
								mainAppController.StartHasBeenPressed();
							    mainAppController.ShowThreeDModel();
								mainAppController.EnableMoreInfoButton();
								mainAppController.EnableHotWheelsBuyNowButton();
								mainAppController.EnableLaunchButton();
								mainAppController.soundController.PlayIdleSound();
								break;
							case "launch button":
								mainAppController.DisableLaunchButton();
								mainAppController.DisableMoreInfoButton();
								mainAppController.DisableSlowMoButton();
								mainAppController.DisableHotWheelsBuyNowButton();
								mainAppController.HideSlowMoModel();
								mainAppController.ShowThreeDModel();
								StartCoroutine(mainAppController.PlayModelAnimation());
								mainAppController.soundController.PlayRaceSound();
								break;
							case "more info button":
								Application.OpenURL(mattelURL);
								break;
							case "slow mo button":
								mainAppController.DisableLaunchButton();
								mainAppController.DisableMoreInfoButton();
								mainAppController.DisableSlowMoButton();
								mainAppController.HideThreeDModel();
								mainAppController.ShowSlowMoModel();
								mainAppController.DisableHotWheelsBuyNowButton();
								StartCoroutine(mainAppController.PlaySlowMoAnimation());
								mainAppController.soundController.PlayRaceSound();
								break;
							case "barbie buy now button": 
								Application.OpenURL(barbieBuyURL);
								break;
							case "hot wheels buy now button": 
								Application.OpenURL(hotwheelsBuyURL);
								break;
							
						}//end of switch
						
						//if(touchCaptured) return;
						
					}//end of if
					
				}//end of loop to cehck gui texture hits
				
				
			}//end of if
			
		}//end of loop goign over touches
		
	}
	
	private bool GUIHitTest(GUITexture theTexture, Vector3 touchPosition) {
		return theTexture.HitTest(touchPosition);
	}
	
}
