using UnityEngine;
using System.Collections;

public class BarbieAppController : MonoBehaviour {
		
	
	/**
	 * ------------------------------------------------------------------
	 * Barbie Vars
	 * ------------------------------------------------------------------
	 **/
	public bool barbieModelEnabled = true;
	public GameObject barbieModel;
	
	public GUITexture barbieBuyNowButton;
	private bool barbieBuyNowButtonEnabled = false;
		
	
	/**
	 * ------------------------------------------------------------------
	 * Scene Vars
	 * ------------------------------------------------------------------
	 **/
	
	public int framesPerSecond = 30;
	
	public BarbieTrackableEventHandler trackEventHandler;
	
	// Use this for initialization
	void Start () {
		
		//HideBarbieModel();
		//barbieBuyNowButton.enabled = barbieBuyNowButtonEnabled;
		barbieBuyNowButton.enabled = false;
		
		// rewind barbie animation
		// barbieModel.animation.Rewind();
		// barbieModel.animation.Stop();
	}
	
	// Update is called once per frame
	void Update () {
		
		//AnimationState animState = barbieModel.animation["Take 001"];
		
		//float curAnimTime = animState.time;
		//float curAnimFrame = curAnimTime * framesPerSecond;
		//float roundedFrame = Mathf.Round(curAnimFrame);
		//float waveLoopStart = 150/framesPerSecond;
		
		// loop waving animation
		//if ( roundedFrame == 190 ) {
		//	barbieModel.animation["Take 001"].time = waveLoopStart;
		//	barbieModel.animation.Play();
		//}
		
		//Debug.Log ("CurrentTime: " + curAnimTime);
		//Debug.Log("currentFrame: " + roundedFrame);
		
	}
	
	
	
	/**
	 * -------------------------------------------------------------------------
	 * Barbie AR Methods
	 * ------------------------------------------------------------------------- 
	 **/
	
	public void ShowBarbieModel(){
		Renderer[] renderComponents = barbieModel.GetComponentsInChildren<Renderer>();
		foreach(Renderer r in renderComponents){
			r.enabled = true;
		}
		//iTween.FadeFrom(barbieModel, iTween.Hash("alpha", 1f,"amount", 0f, "time", 0.75f, "oncomplete", "AnimateBarbra"));
		//AnimateBarbra();
		
		// animate right away
		barbieModel.animation.Rewind();
		barbieModel.animation.wrapMode = WrapMode.ClampForever;
		barbieModel.animation.Play();
	}
		
	public void AnimateBarbra(){
		barbieModel.animation.Rewind();
		barbieModel.animation.wrapMode = WrapMode.ClampForever;
		barbieModel.animation.Play();
	}
	
	public void HideBarbieModel(){
		//barbieModel.animation.Stop();
		Renderer[] renderComponents = barbieModel.GetComponentsInChildren<Renderer>();
		foreach(Renderer r in renderComponents){
			r.enabled = false;
		}
	}
	
	
	/**
	 * -------------------------------------------------------------------------
	 * Barbie GUI Button Methods
	 * ------------------------------------------------------------------------- 
	 **/
	
	public void DisableBarbieBuyNowButton() {
		if ( barbieBuyNowButtonEnabled ) {
			barbieBuyNowButton.enabled = false;
			barbieBuyNowButtonEnabled = false;
		}
	}
	
	public void EnableBarbieBuyNowButton() {
		if ( !barbieBuyNowButtonEnabled	 ) {
			barbieBuyNowButton.enabled = true;
			barbieBuyNowButtonEnabled = true;
		}
	}
	
	
	
	public void StartExperiance(){
		
	}
	
}
