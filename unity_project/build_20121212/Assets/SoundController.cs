using UnityEngine;
using System.Collections;

public class SoundController : MonoBehaviour {
	
	
    public AudioClip[] clips = new AudioClip[4];
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(audio.volume == 0){
			audio.Stop();
		}
	}
	
	public void PlayIntroSound(){
		
		if(!audio.isPlaying){
			audio.volume = 1;
			audio.clip = clips[2];
			audio.loop = false;
			audio.Play();
		} else {
			iTween.AudioTo(this.gameObject, iTween.Hash("volume", 0f, "time", 0.2f, "oncomplete", "PlayIntroSound"));	
		}
	
	}
	
	public void PlayIdleSound(){
		
		if(!audio.isPlaying){
			audio.volume = 1;
			audio.clip = clips[0];
			audio.loop = true;
			audio.Play();
		} else {
			iTween.AudioTo(this.gameObject, iTween.Hash("volume", 0f, "time", 0.2f, "oncomplete", "PlayIdleSound"));	
		}
		
	}
	
	public void PlayRaceSound(){
		
		if(!audio.isPlaying){
			audio.volume = 1;
			audio.clip = clips[1];
			audio.loop = false;
			audio.Play();
		} else {
			iTween.AudioTo(this.gameObject, iTween.Hash("volume", 0f, "time", 0.2f, "oncomplete", "PlayRaceSound"));	
		}
	
	}
	
	public void PlayCrashSound(){
		
		if(!audio.isPlaying){
			audio.volume = 1;
			audio.clip = clips[3];
			audio.loop = false;
			audio.Play();
		} else {
			iTween.AudioTo(this.gameObject, iTween.Hash("volume", 0f, "time", 0.2f, "oncomplete", "PlayCrashSound"));	
		}
	
	}
	
	public void StopAudio() {
		if (audio.isPlaying) {
			audio.Stop();
		}
	}
	
}
